package com.kyejoe;

import java.awt.*;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.concurrent.*;
import javax.swing.JPanel;

import com.kyejoe.creatures.*;
import com.kyejoe.events.GameEvent;
import com.kyejoe.levels.*;

/**
 * The Class GameComponent is the JPanel that manages the data structures for
 * all objects that are currently being updated, as well as the drawing of
 * everything in the game. It is a singleton object, as only one GameComponent
 * will ever be used, and to facilitate it being accessed statically.
 * 
 * @author Joseph Targia, with some additions by Kye Shibata
 */
@SuppressWarnings("serial")
public final class GameComponent extends JPanel {

	/** The Constant frame refers to the singleton GameComponent object. */
	public static final GameComponent frame = new GameComponent();

	/**
	 * The game objects is the ArrayList of all objects currently being updated.
	 */
	private static ArrayList<GameObject> gameObjects;

	/**
	 * The game events is the ArrayList of all events currently being updated.
	 */
	private static ArrayList<GameEvent> gameEvents;

	/**
	 * The background refers to the currently active level object, which
	 * includes a list of objects to be drawn, as well as background images and
	 * enemies.
	 */
	private Level background;

	/**
	 * The main updater is the updater object responsible for running the game
	 * loop.
	 */
	private Updater mainUpdater;

	/**
	 * The title screen is a Level object that is accessed at the beginning of
	 * the game, as well as whenever you win or lose.
	 */
	private TitleScreen screen = new TitleScreen();

	/** The game time score is the score of the current run */
	private long gameTimeScore = 0;

	/**
	 * The game time scores is an array of the top 10 scores, stored in the file
	 * system for later access.
	 */
	private long[] gameTimeScores = ResourceLoader.readScores();

	/**
	 * This integer refers to the level state, 0 for title screen, 1 for in
	 * game, 3 for winning/losing.
	 */
	private int levelNum = 0;

	/**
	 * The mogo is the static Mogo, which is the main object that you control as
	 * a player.
	 */
	private Mogo mogo;

	/**
	 * The pool is the thread pool that updates 30 times per second, drawing
	 * each object on the screen.
	 */
	private ScheduledExecutorService pool;

	/** True if the game is paused. */
	private boolean paused = false;

	/**
	 * Instantiates a new GameComponent. The constructor is private, because it
	 * is a singleton class intended to be instantiated only once. It
	 * initialized the instance fields, and loads the first level
	 */
	private GameComponent() {
		this.setFocusable(true);
		mogo = new Mogo();
		pool = Executors.newScheduledThreadPool(1);
		background = screen;
		this.addMouseListener(screen);
		reset();

	}

	/**
	 * Resets the level so you can play again by clearing the data structures,
	 * and adding the next objects for the latest background. Intended to be
	 * called after loading a new background.
	 */
	public void reset() {

		mainUpdater = new Updater();
		gameObjects = new ArrayList<GameObject>();
		gameEvents = new ArrayList<GameEvent>();
		gameObjects.addAll(background.getLevelObjects());
		gameEvents.addAll(background.getLevelEvents());
		this.setOpaque(true);
		this.setBackground(Color.BLACK);

	}

	/**
	 * Draws each GameObject and the background.
	 * 
	 * @see javax.swing.JComponent#paintComponent(java.awt.Graphics)
	 */
	@Override
	public void paintComponent(Graphics g) {
		Graphics2D g2 = (Graphics2D) g;
		super.paintComponent(g);
		background.draw(g2);
		for (GameObject object : gameObjects) {
			if (!(object.isInvisible()))
				object.draw(g2);
		}
	}

	/**
	 * Pauses the updater, and causes "PAUSED" to be displayed on the screen.
	 */
	public void pause() {
		this.paused = !this.paused;
		mainUpdater.togglePaused();
		background.togglePaused();

	}

	/**
	 * Allows the game to display the next level, or the win or lose screen.
	 * There is currently only 1 possible level besides the Win/Lose/Title
	 * screens.
	 */
	public void nextLevel() {
		levelNum++;
		gameObjects.clear();
		if (levelNum == 1) {
			background = new FirstLevel();
			this.reset();
			this.removeMouseListener(screen);
			addKeyListener(new MovementListener());
			getMogo().heal();
			pool.scheduleAtFixedRate(mainUpdater, 0, 36, TimeUnit.MILLISECONDS);
			gameTimeScore = 0;

		} else if (levelNum == 2) {

			background = screen;
			this.addMouseListener(screen);
			this.reset();
			pool.shutdownNow();
			pool = Executors.newScheduledThreadPool(1);
			levelNum = 0;
		}

	}

	/**
	 * This method shows the winning screen, and updates the list of high scores
	 * if the current score is good enough.
	 */
	public void wonGame() {
		screen.setWin();
		for (int i = 9; i >= 0; i++) {
			if (gameTimeScores[i] > gameTimeScore) {
				gameTimeScores[i] = gameTimeScore;
				break;
			}
		}
		try {
			ResourceLoader.writeScore(gameTimeScores);
		} catch (IOException e) {
			e.printStackTrace();
		}
		nextLevel();
	}

	/**
	 * This ends the game and show the title screen (with a losing message).
	 */
	public void lostGame() {
		screen.setLose();
		mogo = new Mogo();
		nextLevel();
	}

	/**
	 * Gets the game time scores.
	 * 
	 * @return the game time scores
	 */
	public long[] getGameTimeScores() {
		return gameTimeScores;
	}

	/**
	 * Gets the game objects.
	 * 
	 * @return the game objects
	 */
	public synchronized Collection<GameObject> getGameObjects() {
		return gameObjects;
	}

	/**
	 * Gets the game objects.
	 * 
	 * @return the game objects
	 */
	public synchronized Collection<GameEvent> getGameEvents() {
		return gameEvents;
	}

	/**
	 * Gets the background.
	 * 
	 * @return the background.
	 */
	public Level getBack() {
		return background;
	}

	/**
	 * Gets the mogo.
	 * 
	 * @return the mogo
	 */
	public Mogo getMogo() {
		return mogo;
	}

	/**
	 * Gets the game time score.
	 * 
	 * @return the game time score
	 */
	public long getGameTimeScore() {
		return gameTimeScore;
	}

	/**
	 * Increments the game time score.
	 */
	public void incGameTimeScore() {
		gameTimeScore++;
	}
}
