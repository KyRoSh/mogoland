package com.kyejoe.levels;

// TODO: Auto-generated Javadoc
/**
 * The Class HarmfulPlatform is platform. It differs from platforms only in that
 * it is considered to harm mogo if he collides with it. Examples include
 * spikes.
 * 
 * @author Joseph Targia
 */
public class HarmfulPlatform extends Platform {

	/**
	 * Instantiates a new harmful platform.
	 * 
	 * @param fileLocation the file location
	 * @param xLoc the x location.
	 * @param yLoc the y location.
	 */
	public HarmfulPlatform(String fileLocation, int xLoc, int yLoc) {
		super(fileLocation, xLoc, yLoc);
	}

}
