package com.kyejoe.levels;

import com.kyejoe.GameObject;

// TODO: Auto-generated Javadoc
/**
 * The Class Sign is a small non-collideable GameObject. It is the only one that
 * is not collided, and is used for the novelty "walrus sign" at the far end of
 * the level.
 * 
 * @author Kye Shibata
 */
public class Sign extends GameObject {

	/**
	 * Instantiates a new sign.
	 * 
	 * @param xLoc the x location.
	 * @param yLoc the y location.
	 */
	public Sign(int xLoc, int yLoc) {
		super("res/sign.png", xLoc, yLoc);
		getHitBox().setFrame(0, 0, 0, 0);
	}

	/**
	 * The sign does nothing so its location is not updated beyond the usual
	 * shifting done in Updater.
	 * 
	 * @see com.kyejoe.GameObject#updateLocation()
	 */
	@Override
	public void updateLocation() {

	}

}
