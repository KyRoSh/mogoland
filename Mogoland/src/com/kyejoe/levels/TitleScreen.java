package com.kyejoe.levels;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import com.kyejoe.GameComponent;
import com.kyejoe.ResourceLoader;
import com.kyejoe.levels.Level;

// TODO: Auto-generated Javadoc
/**
 * The Class TitleScreen displays the initial screen before the game loads. It
 * also implements a mouse listener to respond to mouse events
 * 
 * @author Joseph Targia
 */
public class TitleScreen extends Level implements MouseListener {

	/** The start button rectangle location. */
	private Rectangle2D startRectangle;

	/** The start button image. */
	private BufferedImage startButton;
	
	private BufferedImage notification;

	/** The height of the screen. */
	private int height;

	/** The width of the screen. */
	private int width;

	/**
	 * Instantiates a new title screen with a start button centered on the
	 * screen.
	 */
	public TitleScreen() {
		super("res/TitleBackground.png");
		height = 500;
		width = 1000;
		startButton = ResourceLoader.loadImage("res/startButton.png");
		// getLevelObjects().add(new Platform("res/startButton.png", 300,300));
		startRectangle = new Rectangle2D.Double(
				(width / 2 - startButton.getWidth() / 2),
				(height / 2 - startButton.getHeight() / 2),
				startButton.getWidth(), startButton.getHeight());
	}

	/**
	 * Draw.
	 *
	 * @param g2 the g2
	 */
	public void draw(Graphics2D g2) {
		super.draw(g2);
		g2.drawImage(startButton, (int) startRectangle.getX(),
				(int) startRectangle.getY(), null);
		if(notification != null) {
		g2.drawImage(notification, (int) (width / 2 - notification.getWidth() / 2),
				10, null);
		}
		long[] scores = GameComponent.frame.getGameTimeScores();
		g2.setFont(new Font("Serif", Font.BOLD, 18));
		g2.setPaint(Color.GREEN);
		g2.drawString("HIGH SCORES \n (lower is better)", 28, 172);
		for(int i = 0; i < 10; i++) {
			if(scores[i] < Long.MAX_VALUE) {
			g2.drawString((i+1)+". "+scores[i], 28, 192+20*i);
			}
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.MouseListener#mouseClicked(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		if (startRectangle.contains(e.getPoint())) {
			GameComponent.frame.nextLevel();
		}
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.MouseListener#mousePressed(java.awt.event.MouseEvent)
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * java.awt.event.MouseListener#mouseReleased(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.MouseListener#mouseEntered(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.awt.event.MouseListener#mouseExited(java.awt.event.MouseEvent)
	 */
	@Override
	public void mouseExited(MouseEvent e) {
		// TODO Auto-generated method stub

	}

	/**
	 * Sets the background to the victory image!
	 */
	public void setWin() {
		notification = ResourceLoader.loadImage("res/youwin.png");		
	}

	/**
	 * Sets the background to the losing image...
	 */
	public void setLose() {
		notification = ResourceLoader.loadImage("res/youlose.png");	
	}

}
