package com.kyejoe.levels;

import java.awt.Graphics2D;

import com.kyejoe.GameComponent;
import com.kyejoe.creatures.*;
import com.kyejoe.events.WalrusFrenzy;

/**
 * The Class FirstLevel is the Grass level that greets Mogo on first playing. It
 * is fully implemented here in this class, as each obstacle was hand placed in
 * its coordinate location
 * 
 * @author Joseph Targia
 */
public class FirstLevel extends Level {

	private StatusPane statusBar = new StatusPane();

	/**
	 * Instantiates the first level
	 * 
	 * All of the level is added to the levelObjects collection in the super
	 * class, where is it later passed to the updater and used in calculations.
	 * Each object was hand placed in order to design the complex level, thus
	 * the long code
	 */
	public FirstLevel() {
		super("res/grasslandsback.PNG");
		getLevelObjects().add(GameComponent.frame.getMogo());
		GameComponent.frame.getMogo().setxLoc(350);
		GameComponent.frame.getMogo().setyLoc(100);
		getLevelObjects().add(new Rhino(0, 0));
		getLevelObjects().add(new Bee(400, 150));
		getLevelObjects().add(new Platform("res/leftWall.png", -500, 0));
		getLevelObjects().add(new Platform("res/leftWall.png", 6000, 0));
		getLevelObjects().add(new Platform("res/rock2.png", 500, 342));
		getLevelObjects().add(new Platform("res/rock.png", 638, 342));
		getLevelObjects().add(new Platform("res/rock2.png", 638, 284));
		getLevelObjects().add(new Platform("res/rock.png", 776, 342));
		getLevelObjects().add(new Platform("res/rock.png", 776, 284));
		getLevelObjects().add(new Platform("res/rock2.png", 776, 226));
		getLevelObjects().add(new Platform("res/rock.png", 914, 342));
		getLevelObjects().add(new Platform("res/rock.png", 914, 284));
		getLevelObjects().add(new HarmfulPlatform("res/spikes.png", 1052, 370));
		getLevelObjects().add(new HarmfulPlatform("res/spikes.png", 1142, 370));
		getLevelObjects().add(new Platform("res/leaf.png", 1180, 320));
		getLevelObjects().add(new HarmfulPlatform("res/spikes.png", 1232, 370));
		getLevelObjects().add(new Platform("res/rock.png", 1322, 342));
		getLevelObjects().add(new Platform("res/rock.png", 1500, 342));
		getLevelObjects().add(new Platform("res/rock2.png", 1638, 342));
		getLevelObjects().add(new Platform("res/rock.png", 1638, 284));
		getLevelObjects().add(new Platform("res/rock.png", 1776, 342));
		getLevelObjects().add(new Platform("res/rock.png", 1776, 284));
		getLevelObjects().add(new Platform("res/rock2.png", 1776, 226));
		getLevelObjects().add(new HarmfulPlatform("res/spikes.png", 1910, 370));
		getLevelObjects().add(new HarmfulPlatform("res/spikes.png", 2000, 370));
		getLevelObjects().add(new HarmfulPlatform("res/spikes.png", 2090, 370));
		getLevelObjects().add(new HarmfulPlatform("res/spikes.png", 2180, 370));
		getLevelObjects().add(new HarmfulPlatform("res/spikes.png", 2270, 370));
		getLevelObjects().add(new Platform("res/leaf.png", 2070, 320));
		getLevelObjects().add(new Platform("res/leaf.png", 2260, 320));
		getLevelObjects().add(new Bee(2500, 250));
		getLevelObjects().add(new PillBug(2800, 200));
		getLevelObjects().add(new PillBug(2950, 200));
		getLevelObjects().add(new Bee(2900, 250));
		getLevelObjects().add(new Platform("res/rock2.png", 3000, 342));
		getLevelObjects().add(new Platform("res/rock2.png", 3138, 342));
		getLevelObjects().add(new Platform("res/rock.png", 3276, 342));
		getLevelObjects().add(new Bee(3400, 250));
		getLevelObjects().add(new Rhino(3450, 0));
		getLevelObjects().add(new Platform("res/leaf.png", 3820, 365));
		getLevelObjects().add(new HarmfulPlatform("res/spikes.png", 3910, 370));
		getLevelObjects().add(new Platform("res/leaf.png", 3940, 320));
		getLevelObjects().add(new HarmfulPlatform("res/spikes.png", 4000, 370));
		getLevelObjects().add(new HarmfulPlatform("res/spikes.png", 4090, 370));
		getLevelObjects().add(new Platform("res/leaf.png", 4110, 320));
		getLevelObjects().add(new HarmfulPlatform("res/spikes.png", 4180, 370));
		getLevelObjects().add(new Platform("res/leaf.png", 4240, 330));
		getLevelObjects().add(new HarmfulPlatform("res/spikes.png", 4270, 370));
		setFinishPoint(new Portal(4500, 200));
		
		getLevelObjects().add(new Sign(4900, 300));
		
		getLevelObjects().add(getFinishPoint());
		
		getLevelEvents().add(new WalrusFrenzy(5500,0));
	}
	

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kyejoe.levels.Level#draw(java.awt.Graphics2D)
	 */
	@Override
	public synchronized void draw(Graphics2D g2) {
		super.draw(g2);
		statusBar.paint(g2);
	}

	@Override
	public void togglePaused() {
		super.togglePaused();
		statusBar.togglePaused();
	}

	

	
}
