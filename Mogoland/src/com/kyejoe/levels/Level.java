package com.kyejoe.levels;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.util.ArrayList;

import com.kyejoe.GameComponent;
import com.kyejoe.GameObject;
import com.kyejoe.ResourceLoader;
import com.kyejoe.events.GameEvent;

/**
 * The Class Level is a super class for all level screen. It handles scrolling
 * of background images, the placing of objects into the screen, drawing, and
 * ending levels.
 * 
 * @author Joseph Targia
 */
public abstract class Level {

	/** The background image for this level. */
	private BufferedImage picture;

	/** The x loc. */
	protected int xLoc = 0;

	/** The level objects. */
	private ArrayList<GameObject> levelObjects = new ArrayList<GameObject>();
	
	/** The level events. */
	private ArrayList<GameEvent> levelEvents = new ArrayList<GameEvent>();


	/** The finish point. */
	private Portal finishPoint;

	/** The x shift. */
	private int xShift = 0;

	private boolean paused = false;

	/**
	 * Instantiates a new level.
	 * 
	 * @param filename the filename
	 */
	public Level(String filename) {
		this.xLoc = 0;
		this.setPicture(ResourceLoader.loadImage(filename));
	}

	/**
	 * Draws the image relative to the amount of scrolling this maintains a
	 * smooth repeat of the background image endlessly in either direction left
	 * or right.
	 * 
	 * @author Joseph Targia
	 * @param g2 the g2
	 */
	public void draw(Graphics2D g2) {
		int w = GameComponent.frame.getWidth();
		int h = GameComponent.frame.getHeight();
		if (xShift >= 0) {
			g2.drawImage(picture, xShift, 0, w, h, 0, 0, w - xShift, h, null);
			g2.drawImage(picture, 0, 0, xShift, h, picture.getWidth() - xShift,
					0, picture.getWidth(), h, null);
		} else if (xShift > w - picture.getWidth()) {
			g2.drawImage(picture, 0, 0, w + xShift, h, -xShift, 0, w, h, null);
			g2.drawImage(picture, w + xShift, 0, w, h, w, 0, w - xShift, h,
					null);
		} else { //

			int negShift = (xShift - (w - picture.getWidth()));
			g2.drawImage(picture, 0, 0, w + negShift, h,
					-negShift + picture.getWidth() - w, 0, picture.getWidth(),
					h, null);
			g2.drawImage(picture, w + negShift, 0, w, h, 0, 0, -negShift, h,
					null);
		}

	}

	/**
	 * Gets the picture.
	 * 
	 * @return the picture
	 */
	public BufferedImage getPicture() {
		return picture;
	}

	/**
	 * Sets the picture.
	 * 
	 * @param picture the new picture
	 */
	public void setPicture(BufferedImage picture) {
		this.picture = picture;
	}

	/**
	 * Gets the x location.
	 * 
	 * @return the x location
	 */
	public int getxLoc() {
		return xLoc;
	}

	/**
	 * Sets the x location.
	 * 
	 * @param xLoc the new x location
	 */
	public void setxLoc(int xLoc) {
		this.xLoc = xLoc;
	}

	/**
	 * Shifts the image in the direction and magnitude of x change.
	 * 
	 * @param xChange the x change
	 */
	public void shift(int xChange) {
		xShift = (xShift - xChange) % picture.getWidth();

	}

	/**
	 * Gets the level objects, which is the ArrayList of all objects associated
	 * with the level, it is the "map".
	 * 
	 * @return the level objects
	 */
	public ArrayList<GameObject> getLevelObjects() {
		return levelObjects;
	}

	/**
	 * Gets the level objects, which is the ArrayList of all events associated
	 * with the level, it is the "map".
	 * 
	 * @return the level event
	 */
	public ArrayList<GameEvent> getLevelEvents() {
		return levelEvents;
	}
	
	/**
	 * Gets the finish point associated with this level, which is the ending
	 * Portal
	 * 
	 * @return the finish point
	 */
	public Portal getFinishPoint() {
		return finishPoint;
	}

	/**
	 * Sets the finish point to the given ending Portal
	 * 
	 * @param finishPoint the new finish point
	 */
	public void setFinishPoint(Portal finishPoint) {
		this.finishPoint = finishPoint;
	}

	public void togglePaused() {
		this.paused = !this.paused;

	}
}
