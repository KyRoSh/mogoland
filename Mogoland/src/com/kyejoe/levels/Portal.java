package com.kyejoe.levels;

import java.awt.geom.Rectangle2D;

import com.kyejoe.levels.Platform;

// TODO: Auto-generated Javadoc
/**
 * The Class Portal is a platform that designates where a level ends. It has an
 * "end box" which is similar to a hit box, but allows collision.
 * 
 * @author Joseph Targia
 */
public class Portal extends Platform {

	/**
	 * The end box is the Rectangle2D representing the location that will end
	 * the level
	 */
	private Rectangle2D endBox;

	/**
	 * Gets the end box representing the location that will end the level.
	 * 
	 * @return the end box
	 */
	public Rectangle2D getEndBox() {
		return endBox;
	}

	/**
	 * Instantiates a new Portal to end a level.
	 * 
	 * @param xLoc the x location
	 * @param yLoc the y location
	 */
	public Portal(int xLoc, int yLoc) {
		super("res/finishPoint.png", xLoc, yLoc);
		endBox = new Rectangle2D.Double();
		endBox.setFrameFromCenter(hitBox.getCenterX(), hitBox.getCenterY(),
				hitBox.getCenterX() + hitBox.getWidth() * .32,
				hitBox.getCenterY() + hitBox.getHeight() * .32);
		this.getHitBox().setFrame(0, 0, 0, 0);
	}

	/**
	 * Ensures that the end box location is probably shifted on each update.
	 * @see com.kyejoe.levels.Platform#updateLocation()
	 */
	@Override
	public void updateLocation() {
		endBox.setFrame(xLoc, yLoc, endBox.getWidth(), endBox.getHeight());
	}
}
