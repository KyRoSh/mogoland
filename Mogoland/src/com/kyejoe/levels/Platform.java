package com.kyejoe.levels;

import com.kyejoe.GameObject;

// TODO: Auto-generated Javadoc
/**
 * The Class Platform is a simple game object that does not move, other than
 * scrolling with the screen. They have hitboxes and are collideable.
 * 
 * @author Kye Shibata
 */
public class Platform extends GameObject {

	/**
	 * Instantiates a new platform, using the image at the given file location.
	 * 
	 * @param fileLocation the file location
	 * @param xLoc the x location.
	 * @param yLoc the y location.
	 */
	public Platform(String fileLocation, int xLoc, int yLoc) {
		super(fileLocation, xLoc, yLoc);
		
	}

	/** 
	 * 
	 * @see com.kyejoe.GameObject#updateLocation()
	 */
	public void updateLocation() {
		this.getHitBox().setFrame(xLoc, yLoc, this.getHitBox().getWidth(),
				this.getHitBox().getHeight());

	}

}
