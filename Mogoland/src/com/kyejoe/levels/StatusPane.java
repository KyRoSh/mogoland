package com.kyejoe.levels;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;

import com.kyejoe.GameComponent;
import com.kyejoe.creatures.Mogo;

// TODO: Auto-generated Javadoc
/**
 * The Class StatusPane is responsible for displaying information at the bottom
 * of the screen. It draws the health bar and displays the game score, as well
 * as indicating a paused state. It can be added to any level and drawn.
 * 
 * @author originally Joseph Targia, Kye Shibata implemented the health bar.
 */
public class StatusPane {

	/** The container. */
	Rectangle2D container = new Rectangle2D.Double(0, 460, 1000, 40);

	/** The paused. */
	private boolean paused = false;

	/**
	 * Checks if is paused.
	 * 
	 * @return true, if is paused
	 */
	public boolean isPaused() {
		return paused;
	}

	/**
	 * Toggle paused.
	 */
	public void togglePaused() {
		this.paused = !this.paused;

	}

	/**
	 * Paint.
	 * 
	 * @param g2 the g2
	 */
	public void paint(Graphics2D g2) {
		g2.setPaint(Color.BLACK);
		g2.fill(container);
		g2.draw(container);
		g2.setFont(new Font("San Serif", Font.BOLD, 20));
		g2.setPaint(Color.RED);
		Mogo mogo = GameComponent.frame.getMogo();

		Rectangle2D healthBar = new Rectangle2D.Double(200, 475,
				((double) mogo.getHealth()) / ((double) mogo.getMaxHealth())
						* 100, 15);
		g2.fill(healthBar);

		g2.setPaint(Color.GREEN);
		g2.drawString(
				"Game Time Score:" + GameComponent.frame.getGameTimeScore(),
				600, 488);
		if (paused) {
			g2.setFont(new Font("San Serif", Font.BOLD, 40));
			g2.setPaint(Color.RED);
			g2.drawString("PAUSED", 10, 493);
		}

		g2.setPaint(Color.WHITE);
		g2.setStroke(new BasicStroke(4));
		Rectangle2D healthBox = new Rectangle2D.Double(200, 475, 100, 15);
		g2.draw(healthBox);

	}

}
