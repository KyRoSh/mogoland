package com.kyejoe;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

import javax.imageio.ImageIO;

// TODO: Auto-generated Javadoc
/**
 * The Class ResourceLoader is a convenience class for loading images from the
 * file system.
 * 
 * @author Joseph Targia implemented all methods, except for the loadSprites
 *         method, which was implemented by Kye Shibata
 */
public class ResourceLoader {

	/**
	 * A static method that loads an image, checking for errors, given a file
	 * location.
	 * 
	 * @param fileLocation the file location
	 * @return the buffered image
	 */
	public static BufferedImage loadImage(String fileLocation) {
		BufferedImage pic = null;
		try {
			pic = ImageIO.read(new File(fileLocation));
		} catch (IOException e) {
			e.printStackTrace();
		}
		return pic;
	}

	/**
	 * Load sprites from a folder.
	 * 
	 * @param path the path
	 * @return the buffered image[][]
	 * @author Kye Shibata
	 */
	public static BufferedImage[][] loadSprites(String path) {

		File[] fileList = new File(path).listFiles();
		BufferedImage[][] picSet2D = new BufferedImage[2][fileList.length / 2];
		int index = 0;

		for (int i = 0; i < 2; i++) {
			for (int j = 0; j < picSet2D[0].length; j++) {
				picSet2D[i][j] = ResourceLoader.loadImage(fileList[index]
						.toString());
				index++;
			}
		}

		return picSet2D;
	}

	/**
	 * Writes the current high scores to the text file "scores.txt" in the root
	 * path.
	 * 
	 * @param gameTimeScores the game time scores
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	public static void writeScore(long[] gameTimeScores) throws IOException {
		String filelocation = "scores.txt";
		File file = new File(filelocation);
		if (!file.exists()) {
			file.createNewFile();
		}
		Arrays.sort(gameTimeScores);

		FileWriter out = null;
		try {
			out = new FileWriter(filelocation);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		int i = 0;
		try {
			while (i < 10) {
				out.write("\n" + gameTimeScores[i]);
				i++;
			}
		} finally {
			out.close();
		}

	}

	/**
	 * Reads the scores from the file system. If no scores exist, a dummy file
	 * is created.
	 * 
	 * @return the array of scores of length 10.
	 */
	public static long[] readScores() {
		long[] top10 = new long[10];
		String filelocation = "scores.txt";
		Scanner scan = null;
		File file = new File(filelocation);
		if (!file.exists()) {
			try {
				file.createNewFile();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		try {
			scan = new Scanner(file);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		int i = 0;
		while (i < 10) {
			if (scan.hasNextLong()) {
				top10[i] = scan.nextLong();
			} else
				top10[i] = Long.MAX_VALUE;
			i++;
		}
		Arrays.sort(top10);
		return top10;

	}

}