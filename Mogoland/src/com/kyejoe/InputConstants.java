package com.kyejoe;

import java.awt.event.KeyEvent;

// TODO: Auto-generated Javadoc
/**
 * The Class InputConstants is a convenience class of constants for key press names. If
 * controls want to be changed, they only need to be changed here.
 * 
 * @author Joseph Targia
 */
public class InputConstants {

	/** The Constant LEFT. */
	public static final int LEFT = KeyEvent.VK_LEFT;

	/** The Constant RIGHT. */
	public static final int RIGHT = KeyEvent.VK_RIGHT;

	/** The Constant UP. */
	public static final int UP = KeyEvent.VK_UP;

	/** The Constant DOWN. */
	public static final int DOWN = KeyEvent.VK_DOWN;

	/** The Constant JUMP. */
	public static final int JUMP = KeyEvent.VK_SPACE;

	/** The Constant SHOOT. */
	public static final int SHOOT = KeyEvent.VK_Z;

	/** The Constant SWITCH. */
	public static final int SWITCH = KeyEvent.VK_X;

	/** The Constant ESC. */
	public static final int ESC = KeyEvent.VK_ESCAPE;

	/**
	 * Private constructor so this cannot be instantiated
	 */
	private InputConstants() {
	}

}
