package com.kyejoe;

import static com.kyejoe.GameComponent.frame;

import java.awt.Graphics2D;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

import com.kyejoe.creatures.Creature;
import com.kyejoe.creatures.Mogo;
import com.kyejoe.levels.HarmfulPlatform;
import com.kyejoe.weapons.Attack;

/**
 * The Class GameObject.
 * 
 * @author Kye Shibata, with some methods by Joseph Targia
 */
public abstract class GameObject {

	/** The currently active picture that will be drawn by the draw method. */
	private BufferedImage picture;

	/** The upper left x location. */
	protected int xLoc = 20;

	/** The upper left y location. */
	protected int yLoc = 20;

	/** The Constant GRAVITY is used in falling to accelerate GameObjects. */
	public static final double GRAVITY = .4;

	/** The hit box is used to calculate collisions between objects */
	protected Rectangle2D hitBox;

	/**
	 * The marked for death flag is used to determine whether an object should
	 * be removed from updates.
	 */
	protected boolean markedForRemoval = false;

	/**
	 * The invisible flag determines whether an object is invisible and thus
	 * should not be drawn
	 */
	private boolean invisible = false;

	/**
	 * Instantiates a new game object, using the designated image, and placing
	 * it at the designated coordinates.
	 * 
	 * @param fileLocation the file location of the image
	 * @param xLoc the x loc
	 * @param yLoc the y loc
	 */
	public GameObject(String fileLocation, int xLoc, int yLoc) {
		this.xLoc = xLoc;
		this.yLoc = yLoc;
		picture = ResourceLoader.loadImage(fileLocation);
		hitBox = new Rectangle2D.Double(xLoc + picture.getWidth(), yLoc
				+ picture.getHeight(), picture.getWidth(), picture.getHeight());
	}

	/**
	 * Updates the hitbox to fit the current image in its current location.
	 */

	public void updateHitBox() {
		hitBox = new Rectangle2D.Double(xLoc + picture.getWidth(), yLoc
				+ picture.getHeight(), picture.getWidth(), picture.getHeight());
	}

	/**
	 * Changes the active image to the specified image.
	 * 
	 * @param image the image to switched to.
	 */
	public void changePic(BufferedImage image) {
		picture = image;
	}

	/**
	 * Gets the active picture.
	 * 
	 * @return the picture
	 */
	public synchronized BufferedImage getPicture() {
		return picture;
	}

	/**
	 * Update location is defined by subclasses, calculating how the object
	 * should be changed on each update.
	 */
	public abstract void updateLocation();

	/**
	 * Gets the upper left x location.
	 * 
	 * @return the x location
	 */
	public synchronized int getxLoc() {
		return xLoc;
	}

	/**
	 * Gets the upper left y location.
	 * 
	 * @return the y location
	 */
	public synchronized int getyLoc() {
		return yLoc;
	}

	/**
	 * Sets the upper left x location.
	 * 
	 * @param x the new x location
	 */
	public void setxLoc(int x) {
		this.xLoc = x;
	}

	/**
	 * Sets the upper left y location.
	 * 
	 * @param y the new y location
	 */
	public void setyLoc(int y) {
		this.yLoc = y;
	}

	/**
	 * Gets the hit box, which is the Rectangle2D object that corresponds to the
	 * coordinates that other GameObjects would collide with.
	 * 
	 * @return the hit box
	 */
	public Rectangle2D getHitBox() {
		return hitBox;
	}

	/**
	 * Tests if this game object will collide with any object in a given set of
	 * of GameObjects, if it were to move a certain number of pixels in the x or
	 * y dimensions
	 * 
	 * @param xMove the x move
	 * @param yMove the y move
	 * @author Joseph Targia
	 * @return true, if successful
	 */
	public boolean willCollide(int xMove, int yMove) {
		if (yLoc + yMove > frame.getHeight() - hitBox.getHeight() - 100)
			return true;
		for (GameObject target : GameComponent.frame.getGameObjects()) {
			if (!this.equals(target) && !(target instanceof Attack)) {
				Rectangle2D.Double nextRec = new Rectangle2D.Double(
						hitBox.getX() + xMove, hitBox.getY() + yMove,
						hitBox.getWidth(), hitBox.getHeight());

				// applies damage to Mogo if something collides with him
				if (nextRec.intersects(target.hitBox)) {
					if (((this instanceof Creature || this instanceof HarmfulPlatform) && target instanceof Mogo)
							|| ((target instanceof Creature || target instanceof HarmfulPlatform) && this instanceof Mogo)) {
						frame.getMogo().kill();
					}

					return true;
				}
			}
		}

		return false;
	}

	/**
	 * Draws the active game object image in the proper location.
	 * 
	 * @param g2 the Graphics2D object
	 * @author Joseph Targia
	 */
	public synchronized void draw(Graphics2D g2) {
		g2.drawImage(getPicture(), getxLoc(), getyLoc(), null, null);
	}

	/**
	 * Sets the markedForDeath flag to true, indicating the object should be
	 * removed by the updater.
	 */
	public synchronized void remove() {
		markedForRemoval = true;
	}

	/**
	 * Kills this object, removing it from being drawn (or reducing its health).
	 */
	public void kill() {
		remove();
	}

	/**
	 * Checks if is marked for death.
	 * 
	 * @return true, if is marked for death
	 */
	public boolean isMarkedForRemoval() {
		return markedForRemoval;
	}

	/**
	 * This method determines the distance from the mogo object. Used by
	 * platforms and creatures that react to mogo.
	 * 
	 * @return the current distance this GameObject is from mogo.
	 */
	public int distanceFromMogo() {
		return Math.abs(this.getxLoc()
				- GameComponent.frame.getMogo().getxLoc());
	}

	/**
	 * Checks if this game object is invisible.
	 * 
	 * @return true, if it is invisible
	 */
	public boolean isInvisible() {
		return invisible;

	}

	/**
	 * Sets the invisible flag of this object, determining whether it will be
	 * drawn or not.
	 * 
	 * @param value the new invisible
	 */
	public void setInvisible(boolean value) {
		invisible = value;
	}
}
