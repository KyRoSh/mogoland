package com.kyejoe.creatures;

/**
 * The Class PillBug is an enemy that rolls around and can shield himself. It
 * can only be hit when it is turned around.
 * 
 * @author Kye Shibata
 */
public class PillBug extends Creature {

	/** The current walk time. */
	private int walkTime = 0;

	/** The walk time left. */
	private int walkTimeLeft = 0;

	/** The speed of PillBug when rolling. */
	private int rollSpeed = 4;

	/** The time he rolls. */
	private int rollTime = 0;

	/** The time spent shielded. */
	private int shielded = 0;

	/**
	 * Instantiates a new pill bug, which is an enemy that rolls around and can
	 * shield himself. It can only be hit when it is turned around.
	 * 
	 * @param xLoc the x location.
	 * @param yLoc the y location.
	 */
	public PillBug(int xLoc, int yLoc) {
		super("res/pillbug/01-standing.png", "pillbug", xLoc, yLoc);
		setHealth(1);
		walkSpeed = 1;
		directionMod = -1;
	}

	/**
	 * The pill bug moves by walking around, and occasionally rolling toward
	 * Mogo. It blocks itself if Mogo attacks its front side.
	 * 
	 * @see com.kyejoe.creatures.Creature#move()
	 */
	public void move() {
		if (!dead) {
			if (shielded > 0) {
				shielded--;
				if (shielded == 0)
					changePic(picSet[0]);
			}

			else if (walkTimeLeft > 0) {
				if (!willCollide(walkSpeed * directionMod, 0))
					xLoc += walkSpeed * directionMod;
				walkTime++;

				if ((walkTime / 5) % 2 == 0) {
					changePic(picSet[1]);
				} else if ((walkTime / 5) % 2 != 0) {
					changePic(picSet[2]);

				}
				walkTimeLeft--;

			}

			else if (rollTime > 0 || Math.random() <= 0.15) {
				roll();
			}

			else if (Math.random() <= 0.3) {
				faceMogo();
			} else if (Math.random() <= 0.4) {
				turn();
				walkTimeLeft = 25;
			} else if (Math.random() <= 0.5) {
				walkTimeLeft = 25;

			}

		}

	}

	/**
	 * This method shields the pill bug from Mogo's attacks.
	 */
	public void shield() {
		shielded = 5;
		changePic(picSet[7]);
	}

	/**
	 * Kills the pillbug if he iss facing away from Mogo, else shields.
	 * 
	 * @see com.kyejoe.creatures.Creature#kill()
	 */
	public void kill() {
		if (!isFacingMogo() && shielded == 0 && rollTime == 0) {
			super.kill();
		} else {
			shield();
		}
	}

	/**
	 * Charges forward in a roll animation.
	 */
	public void roll() {
		if (rollTime == 0)
			rollTime = 20;

		if (!willCollide(rollSpeed * directionMod, 0)) {

			switch (rollTime % 8) {

			case 0:
				changePic(picSet[3]);
				break;
			case 2:
				changePic(picSet[4]);
				break;
			case 4:
				changePic(picSet[6]);
				break;
			case 6:
				changePic(picSet[7]);
				break;
			}

			xLoc += directionMod * rollSpeed;

		}
		rollTime--;

	}

}
