package com.kyejoe.creatures;

import java.awt.image.BufferedImage;

import com.kyejoe.GameComponent;
import com.kyejoe.GameObject;
import com.kyejoe.ResourceLoader;

// TODO: Auto-generated Javadoc
/**
 * The Class Creature is the super class of all creatures, which includes Mogo and all enemies.
 * 
 * 
 * @author originally authored by Joseph Targia, but heavily refactored with animations, death behavior and health by Kye Shibata
 */
public abstract class Creature extends GameObject {
	
	/** The y velocity. */
	protected double yVeloc = 0;
	
	/** The terminal fall velocity. */
	private double termVeloc = 3;
	
	/** true if the creature can jump */
	private boolean canJump = false;
	
	/** The time starting airborne when jumping or falling. */
	private long startAirborne = System.nanoTime();
	
	/** The Constant GRAVITY. */
	public static final double GRAVITY = .1;
	
	/** The jump velocity. */
	private double jumpVel;
	
	/** The direction mod, 1 is right, -1 is left. */
	protected int directionMod = 1;
	
	/** The walk speed. */
	protected int walkSpeed;

	/** The health. */
	private int health;
	
	/** True if the creature is dead. */
	protected boolean dead = false;	
	




	/** The amount of updates since death time. */
	protected int deadTime = 0;
	
	/** The 2D pic set contains a left facing and a right facing array of images. */
	protected BufferedImage[][] picSet2D;	
	
	/** The pic set contains the array of images for the creature. */
	protected BufferedImage[] picSet;
	
	/** Tells if he's a flying enemy.*/
	protected boolean airborne = false;
	
	/** Indicates the creature was just hit */
	protected int justDamaged = 0;

	/**
	 * Instantiates a new creature.
	 *
	 * @param fileLocation the file location
	 * @param picsetLocation the picset location
	 * @param xLoc the x loc
	 * @param yLoc the y loc
	 */
	public Creature(String fileLocation, String picsetLocation, int xLoc, int yLoc) {
		super(fileLocation, xLoc, yLoc);
		picSet2D = ResourceLoader.loadSprites("res/" + picsetLocation);	
		picSet = picSet2D[1];
	}
	

	/**
	 * The abstract method move is specified by each creature, which will move differently depending on the type.
	 */
	public abstract void move();

	/* (non-Javadoc)
	 * @see com.kyejoe.GameObject#updateLocation()
	 */
	public void updateLocation()
	{
		
		if(justDamaged > 0)
		{
			if (justDamaged % 2 == 0)
			{
				setxLoc(xLoc + 2);
			} else {
				setxLoc(xLoc - 2);
			}
			
			justDamaged --;
		} 

		if(dead)
		{			
			if (deadTime == 0)
			{				
				faceMogo();
				directionMod *= -1;
				changePic(picSet[5]);
				setCanJump(true);
				jump(2);
				
			}
			
			deadTime++;		
		
			
			xLoc += directionMod;
				
			if (deadTime > 20 && testGrounded())		
				remove();
		} else if (justDamaged == 0){
			move();
		}

		if (!airborne)
			fall();	
		
		
		this.getHitBox().setFrame(xLoc, yLoc, this.getHitBox().getWidth(),
				this.getHitBox().getHeight());
		
		
	}
	
	/**
	 * During each update, this method applies gravity and moves the creature downward, if there is nothing in the way.
	 */
	public void fall() {

		if (!testGrounded() || !isCanJump()) {
			int yIncrease;
			long current = System.nanoTime();
			
			if (yVeloc < termVeloc)
				yVeloc += (current - startAirborne) / 100000000 * GRAVITY;
			
			else
				yVeloc = termVeloc;
			
			yIncrease = (int) (3 * yVeloc);
			
			if (!willCollide(0, yIncrease)) {
				yLoc += yIncrease;
				
			} else {
				boolean moved = false;
				while (!moved && yIncrease > 0) {
					yIncrease--;
					if (!willCollide(0, yIncrease)) {
						yLoc += yIncrease;
						moved = true;
					}

				}
				setCanJump(testGrounded());
				yVeloc = 0;
			}
		}
	}
	
	

	/**
	 * Adds default upward velocity, which the fall method takes into account
	 */
	public void jump() {
		if (isCanJump()) {// && !willCollide(0,-1,GameComponent.creatures)) {
			setCanJump(false);
			yVeloc -= getJumpVel();
			startAirborne = System.nanoTime();
		}
	}
	
	/**
	 * The jump method adds a specific upward velocity to this creature.
	 *
	 * @param jumpVel the jump velocity
	 */
	public void jump(int jumpVel) {
		if (isCanJump()) {// && !willCollide(0,-1,GameComponent.creatures)) {
			setCanJump(false);
			yVeloc -= jumpVel;
			startAirborne = System.nanoTime();
		}
	}

	/**
	 * Tests if the creature is grounded
	 * 
	 * if it is both grounded and canJump, it will reset the startAirborne time
	 *
	 * @return true, if grounded
	 */
	public boolean testGrounded() {
		if (willCollide(0, 1)) {
			if (isCanJump())
				startAirborne = System.nanoTime();
			return true;
		}
		return false;
	}

	/**
	 * Sets the health of the creature.
	 *
	 * @param health the new health
	 */
	public void setHealth(int health)
	{
		this.health = health;
	}
	
	/**
	 * Gets the health.
	 *
	 * @return the health
	 */
	public int getHealth()
	{
		return health;
	}

	/**
	 * Checks if it can jump.
	 *
	 * @return true, if the creature can jump
	 */
	public boolean isCanJump() {
		return canJump;
	}

	/**
	 * Sets the can jump.
	 *
	 * @param canJump the new can jump
	 */
	public void setCanJump(boolean canJump) {
		this.canJump = canJump;
	}

	/**
	 * Gets the jump velocity.
	 *
	 * @return the jump velocity
	 */
	public double getJumpVel() {
		return jumpVel;
	}

	/**
	 * Sets the jump velocity.
	 *
	 * @param jumpVel the new jump velocity
	 */
	public void setJumpVel(double jumpVel) {
		this.jumpVel = jumpVel;
	}
	
	/* (non-Javadoc)
	 * @see com.kyejoe.GameObject#kill()
	 */
	public void kill()
	{
		health--;
		if(health <= 0)
		{
			dead = true;
			airborne = false;
		}
		else 
			justDamaged = 6;
				
	}
	
	/**
	 * Sets the picture set and current direction toward the direction Mogo is located, relative to this creature
	 */
	public void faceMogo()
	{
		if(GameComponent.frame.getMogo().getxLoc() < xLoc && directionMod > 0) 
		{
			directionMod = -1;
			picSet = picSet2D[1];
		} else if (GameComponent.frame.getMogo().getxLoc() > xLoc && directionMod < 0)
		{
			directionMod = 1;
			picSet = picSet2D[0];
		}
	}
	
	public boolean isFacingMogo()
	{
		int distance = (GameComponent.frame.getMogo().getxLoc() - this.getxLoc());
		return (distance/Math.abs(distance) == directionMod);
	}
	
	
	
	/**
	 * Turn.
	 */
	public void turn()
	{
		directionMod *= -1;
		if(directionMod > 0)
			picSet = picSet2D[0];
		else
			picSet = picSet2D[1];
		
		changePic(picSet[0]);
	}
	
	public boolean isDead() {
		return dead;
	}

	
}
