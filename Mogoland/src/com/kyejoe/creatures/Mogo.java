package com.kyejoe.creatures;

import static com.kyejoe.InputConstants.*;
import java.util.ArrayList;
import java.util.HashSet;

import com.kyejoe.MovementListener;
import com.kyejoe.weapons.Attack;
import com.kyejoe.weapons.Laser;
import com.kyejoe.weapons.WaveyLaser;

// TODO: Auto-generated Javadoc
/**
 * The Class Mogo is the main creature character controlled by the player. It
 * accepts input, and can shoot lasers, which are novel to this creature.
 * 
 * @author Joseph Targia originally implemented with key input, Kye Shibata
 *         implemented all animations and images, Kye implemented the original
 *         laser, while Joseph added the switching between laser
 */
public final class Mogo extends Creature {

	/** The Constant RIGHTWARD. */
	public static final int RIGHTWARD = 1;

	/** The Constant LEFTWARD. */
	public static final int LEFTWARD = -1;

	/** The currently facing direction. */
	private int direction = RIGHTWARD;

	/** True if Mogo is currently in the air. */
	private boolean airborne = true;

	/** The amount of time (in game updates) Mogo has been walking. */
	private int walkTime = 0;

	/** The amount of time (in game updates) since Mogo last shot its weapon. */
	private int justShot = 0;

	/** The maximum health of Mogo, in number of hits. */
	private int maxHealth = 3;

	private boolean inBoss = false;

	/** The amount of time Mogo is invincible for. */
	private int invincible = 0;

	/** The amount of x change in a movement. Used for shifting the screen */
	private int xChange;

	/** The new attacks fired by Mogo. */
	private ArrayList<Attack> newAttacks = new ArrayList<Attack>();

	/** The amount of time (in game updates) since Mogo switched weapons. */
	private int switchCounter = 0;

	/** The weapon index refers to the currently active weapon number. */
	private int weaponIndex = 1;

	/**
	 * Instantiates a new mogo.
	 */
	public Mogo() {
		super("res/mogo/01-standing.png", "mogo", 350, 100);
		setJumpVel(2.5);
		setHealth(maxHealth);
	}

	/**
	 * Gets the new attacks fired by Mogo.
	 * 
	 * @return the new attacks
	 */
	public ArrayList<Attack> getNewAttacks() {
		return newAttacks;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kyejoe.creatures.Creature#updateLocation()
	 */
	@Override
	public synchronized void updateLocation() {

		if (testGrounded()) {
			airborne = false;
		}

		if (invincible > 0) {
			if (invincible % 2 == 0) {
				setInvisible(true);
			} else {
				setInvisible(false);
			}

			invincible--;

		}

		if (justDamaged > 0) {
			if (justDamaged == 3) {
				changePic(picSet[6]);
			}
			if (!willCollide(direction * 2, 0))
				;
			setxLoc(getxLoc() + direction * 2);
			justDamaged--;
		}

		else if (MovementListener.isPressed()) {
			move();
		} else if (!airborne) {
			stand();
			xChange = 0;
		} else {
			xChange = 0;
		}
		if (yVeloc > 0)
			changePic(picSet[3]);
		else if (yVeloc < 0)
			changePic(picSet[4]);

		fall();

		if (justShot > 0) {
			justShot--;
		}
		if (switchCounter > 0) {
			switchCounter--;
		}

		this.getHitBox().setFrame(xLoc, yLoc, this.getHitBox().getWidth(),
				this.getHitBox().getHeight());
	}

	/**
	 * Sets the picture of Mogo to the normal standing picture.
	 */
	public void stand() {
		if (justShot < 18) {
			changePic(picSet[0]);
		}
		walkTime = 0;

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see com.kyejoe.creatures.Creature#move()
	 */
	@Override
	public void move() {
		HashSet<Integer> currentKeys = MovementListener.getCurrentKey();
		int move = 5;

		if (currentKeys.contains(LEFT)) {
			if (!willCollide(-move, 0))
				setxLoc(getxLoc() + -move);

			picSet = picSet2D[1];
			direction = LEFTWARD;

			if (!airborne) {
				walkTime++;
				if ((walkTime / 5) % 2 == 0 || walkTime % 5 == 0) {
					changePic(picSet[1]);
				} else if ((walkTime / 5) % 2 != 0) {
					changePic(picSet[2]);

				}
			}

			xChange = -move;
		} else if (currentKeys.contains(RIGHT)) {
			if (!willCollide(move, 0))
				setxLoc(getxLoc() + move);
			picSet = picSet2D[0];
			direction = RIGHTWARD;
			if (!airborne) {
				walkTime++;
				if ((walkTime / 5) % 2 == 0 && walkTime % 5 == 0) {
					changePic(picSet[1]);
				} else if (walkTime % 5 == 0) {
					changePic(picSet[2]);
				}
			}

			xChange = move;
		} else {
			xChange = 0;
		}

		if (currentKeys.contains(JUMP))
			if (this.isCanJump()) {
				jump();
				airborne = true;
			}
		if (currentKeys.contains(SHOOT)) {

			shoot();
		}

		if (currentKeys.contains(SWITCH)) {
			if (switchCounter == 0) {
				if (weaponIndex == 0) {
					weaponIndex++;
				} else
					weaponIndex--;
				switchCounter = 25;

			}
		}

	}

	/**
	 * Shoots the currently equipped weapon.
	 */
	public void shoot() {
		if (justShot == 0) {
			int nextx;
			int nexty;
			if (direction == RIGHTWARD) {
				nextx = getxLoc() + 25;
				nexty = getyLoc() + 5;

			} else {
				nextx = getxLoc();
				nexty = getyLoc() + 5;
			}

			Attack attack = getWeapon(nextx, nexty);
			changePic(picSet[5]);
			justShot = attack.getRechargeRate();
			this.newAttacks.add(attack);
		}
	}

	/**
	 * Gets the proper newly fired attack, depending on the weapon index.
	 * 
	 * @param nextx the nextx
	 * @param nexty the nexty
	 * @return the weapon
	 */
	private Attack getWeapon(int nextx, int nexty) {
		if (weaponIndex == 0) {
			if (direction < 0)
				return new Laser(nextx - 20, nexty, direction);
			else
				return new Laser(nextx, nexty, direction);
		}
		if (weaponIndex == 1) {
			return new WaveyLaser(nextx, nexty, direction);
		}

		else
			return null;

	}

	/**
	 * Gets the x change of the most recent movement.
	 * 
	 * @return the x change
	 */
	public int getxChange() {
		return xChange;
	}

	/**
	 * This is overrided to do nothing, as Mogo cannot face himself.
	 * 
	 * @see com.kyejoe.creatures.Creature#faceMogo()
	 */
	public void faceMogo() {
		// this is Mogo so the method is overrided to do nothing.
	}

	/**
	 * Heals Mogo to his maximum health.
	 */
	public void heal() {
		setHealth(maxHealth);
	}

	/**
	 * Gets the maximum health (in number of hits).
	 * 
	 * @return the max health
	 */
	public int getMaxHealth() {
		return maxHealth;
	}

	/**
	 * If Mogo is not currently invincible, he is damaged. He then is invincible
	 * for a short time, unless he has fully died.
	 * 
	 * @see com.kyejoe.creatures.Creature#kill()
	 */
	public void kill() {
		if (invincible == 0) {
			super.kill();
			justDamaged = 3;
			if (getHealth() > 0) {
				invincible = 20;
			}
		}
	}

	public void setInBoss(boolean value) {
		inBoss = value;
	}

	public boolean isInBoss() {
		return inBoss;
	}

}
