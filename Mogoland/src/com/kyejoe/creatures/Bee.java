package com.kyejoe.creatures;

// TODO: Auto-generated Javadoc
/**
 * The Class Bee is an enemy that flies around in a circle around a given area.
 * It does not collide with anything but Mogo, which it damages.
 * 
 * @author Kye Shibata
 */
public class Bee extends Creature {

	/** The theta. */
	private double theta = 0;

	/** The radius of the circle of movement. */
	private static int radius = 8;

	/** The speed of the movement of the bee. */
	private int speed = 5;

	/**
	 * Instantiates a new bee, which is a flying creature.
	 * 
	 * @param xLoc the x location.
	 * @param yLoc the y location.
	 */
	public Bee(int xLoc, int yLoc) {
		super("res/bee/00-bee1.png", "bee", xLoc, yLoc);
		setHealth(1);
		directionMod = -1;
		airborne = true;

	}

	/**
	 * The bee moves in a large circle around a point, it can only collide with
	 * Mogo.
	 * 
	 * @see com.kyejoe.creatures.Creature#move()
	 */
	public void move() {
		faceMogo();
		double angle = (360 - theta) * Math.PI / 180.0;

		setxLoc((int) Math.round(getxLoc() + (radius * Math.cos(angle))
				- radius * Math.cos(angle - speed)));
		setyLoc((int) Math.round(getyLoc() + (radius * Math.sin(angle))
				- radius * Math.sin(angle - speed)));

		theta += speed;

		changePic(picSet[((int) (theta / 5)) % 3]);

	}

}
