package com.kyejoe.creatures;

/**
 * The Class Walrus is a very early creature, used primarily for testing
 * purposes. It can be found in secret locations in Mogoland though, such as the
 * WalrusFrenzy event.
 * 
 * @author Joseph Targia
 */
public final class Walrus extends Creature {

	/**
	 * The count is the number of walruses currently alive.
	 */
	private static int count = 0;

	/** The direction. */
	private int direction = -1;

	/**
	 * Instantiates a new walrus.
	 * 
	 * @param xLoc the x location.
	 * @param yLoc the y location.
	 */
	public Walrus(int xLoc, int yLoc) {
		super("res/walrus/1.png", "walrus", xLoc, yLoc);
		setJumpVel(2.4);
		setHealth(3);
		count++;
	}

	/**
	 * The walrus moves by hopping in one direction, and then switching and
	 * hopping in the other direction when it hits something.
	 * 
	 * @see com.kyejoe.creatures.Creature#move()
	 */
	@Override
	public void move() {

		int move = 2 * direction;
		if (!willCollide(move, 0)) {
			xLoc += move;
			jump();
		} else {
			direction = -direction;
			if (!willCollide(move, 0)) {
				xLoc += move;
				jump();
			}
		}

	}

	/**
	 * Kills the walrus when its health is depleted.
	 * 
	 * @see com.kyejoe.creatures.Creature#kill()
	 */
	@Override
	public void kill() {
		super.kill();
		if (getHealth() == 0)
			count--;
	}

	/**
	 * Gets the count used for the walrus frenzy boss event.
	 * 
	 * @return the count
	 */
	public static int getCount() {
		return count;
	}

}
