package com.kyejoe.creatures;

/**
 * The Class Rhino is a creature that charges toward Mogo at very fast speed if
 * he gets close.
 * 
 * @author Kye Shibata
 */
public class Rhino extends Creature {

	/** The walk time. */
	private int walkTime = 0;

	/** The walk time left. */
	private int walkTimeLeft = 0;

	/** The speed of Rhino when charging. */
	private int chargeSpeed = 4;

	/**
	 * Instantiates a new rhino.
	 * 
	 * @param xLoc the x loc
	 * @param yLoc the y loc
	 */
	public Rhino(int xLoc, int yLoc) {
		super("res/rhino/21-standingl.png", "rhino", xLoc, yLoc);
		setJumpVel(2.4);
		setHealth(2);
		walkSpeed = 2;
		directionMod = -1;
	}

	/**
	 * Moves around slowly, unless Mogo comes within range, then it charges
	 * forward.
	 * 
	 * @see com.kyejoe.creatures.Creature#move()
	 */
	public void move() {
		if (!dead) {
			if (distanceFromMogo() < 130
					&& !willCollide(chargeSpeed * directionMod, 0)) {
				charge();
			} else if (walkTimeLeft > 0) {
				if (!willCollide(walkSpeed * directionMod, 0))
					xLoc += walkSpeed * directionMod;
				walkTime++;
				if ((walkTime / 5) % 2 == 0) {
					changePic(picSet[1]);
				} else if ((walkTime / 5) % 2 != 0) {
					changePic(picSet[2]);

				}
				walkTimeLeft--;

			} else if (Math.random() <= 0.2) {
				walkTimeLeft = 15;
			} else if (Math.random() <= 0.2) {
				turn();
			}
		}

	}

	/**
	 * Charges toward Mogo at a fast speed, changing to an angry animation.
	 */
	public void charge() {

		faceMogo();

		if (!willCollide(chargeSpeed * directionMod, 0)) {

			if ((walkTime / 5) % 2 == 0 || walkTime % 5 == 0) {
				changePic(picSet[3]);

			} else if ((walkTime / 5) % 2 != 0) {
				changePic(picSet[4]);

			}

			xLoc += directionMod * chargeSpeed;

			walkTime++;

		}
	}

}
