package com.kyejoe.weapons;

import com.kyejoe.GameComponent;
import com.kyejoe.GameObject;

// TODO: Auto-generated Javadoc
/**
 * The Class Attack is the abstract super class of all attacks that Mogo (or
 * other creatures) can use.
 * 
 * @author Kye Shibata, modified by Joseph Targia
 */
public abstract class Attack extends GameObject {

	/**
	 * The recharge rate in number of game updates. All subclasses must set
	 * this.
	 */
	private int rechargeRate;

	/**
	 * Instantiates a new attack.
	 * 
	 * @param fileLocation the file location
	 * @param xLoc the x loc
	 * @param yLoc the y loc
	 */
	public Attack(String fileLocation, int xLoc, int yLoc) {
		super(fileLocation, xLoc, yLoc);

	}

	/**
	 * Attacks update their location by moving, then determining if they
	 * collided with anything, and killing themselves if so.
	 * 
	 * @see com.kyejoe.GameObject#updateLocation()
	 */
	public synchronized void updateLocation() {
		move();
		this.getHitBox().setFrame(xLoc, yLoc, this.getHitBox().getWidth(),
				this.getHitBox().getHeight());
		if (this.getHitBox().getCenterX() < -50
				|| this.getHitBox().getCenterX() > GameComponent.frame
						.getWidth() + 50) {
			this.remove();
			GameComponent.frame.getMogo().getNewAttacks().clear();

		}
	}

	/**
	 * The move method determines the moving behavior of the Attack, it is
	 * specified in each subclass.
	 */
	public abstract void move();

	/**
	 * Gets the recharge rate of this attack.
	 * 
	 * @return the recharge rate
	 */
	public int getRechargeRate() {
		return rechargeRate;
	}

	/**
	 * Sets the recharge rate of this attack.
	 * 
	 * @param rechargeRate the new recharge rate
	 */
	protected void setRechargeRate(int rechargeRate) {
		this.rechargeRate = rechargeRate;
	}

}
