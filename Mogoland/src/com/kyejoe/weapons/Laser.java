package com.kyejoe.weapons;

import java.awt.geom.Rectangle2D;

import com.kyejoe.GameComponent;
import com.kyejoe.GameObject;
import com.kyejoe.creatures.Mogo;
import com.kyejoe.levels.Platform;

/**
 * The Class Laser is an attack that shoots a red laser in a straight line.
 * 
 * @author originally Kye Shibata, but Joseph Targia modified the speed and
 *         recharge rate implementations.
 */
public class Laser extends Attack {

	/** The laser speed. */
	private int laserSpeed = 36;

	/** The Constant DEFAULTRECHARGE. */
	protected static final int DEFAULTRECHARGE = 25;

	/**
	 * Instantiates a new laser. Use only for default lasers. Subclasses should
	 * use the other constructor
	 * 
	 * @param x the x location of the laser
	 * @param y the y location of the laser
	 * @param direction the direction. -1 is left, 1 is right
	 */
	public Laser(int x, int y, int direction) {
		super("res/flyingLaser.png", x, y);
		laserSpeed *= direction;
		setRechargeRate(DEFAULTRECHARGE);

	}

	/**
	 * Instantiates a new laser. Intended to be used by subclasses.
	 * 
	 * @param pic the String path to the picture for the laser
	 * @param x the x location of the laser
	 * @param y the y location of the laser
	 * @param speed subclasses should return their speed multiplied by direction
	 */
	protected Laser(String pic, int x, int y, int speed) {
		super(pic, x, y);
		laserSpeed = speed;
	}

	/**
	 * Checks whether the laser kills anything, then kills the target and itself
	 * if so.
	 * 
	 * @param xMove the x move
	 */
	public void killCheck(int xMove) {
		for (GameObject target : GameComponent.frame.getGameObjects()) {
			if (!(target instanceof Mogo) && !(target instanceof Attack)) {
				Rectangle2D.Double nextRec = new Rectangle2D.Double(getHitBox()
						.getX() + xMove, getHitBox().getY(), getHitBox()
						.getWidth(), getHitBox().getHeight());

				if (nextRec.intersects(target.getHitBox())) {
					if (!(target instanceof Platform))
						target.kill();
					this.remove();

				}
			}
		}
	}

	/**
	 * The laser moves very quickly in a straight line in one y direction.
	 * 
	 * @see com.kyejoe.weapons.Attack#move()
	 */
	public void move() {
		killCheck(laserSpeed);
		setxLoc(getxLoc() + laserSpeed);
	}

	/**
	 * Gets the laser speed.
	 * 
	 * @return the laser speed
	 */
	public int getLaserSpeed() {
		return laserSpeed;
	}

	/**
	 * Sets the laser speed.
	 * 
	 * @param laserSpeed the new laser speed
	 */
	public void setLaserSpeed(int laserSpeed) {
		this.laserSpeed = laserSpeed;
	}

}
