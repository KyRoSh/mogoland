package com.kyejoe.weapons;

/**
 * The Class WaveyLaser is a laser weapon that moves in an wave pattern.
 * 
 * @author Joseph Targia
 */
public class WaveyLaser extends Laser {

	/** the starting y position . */
	private int initialY;
	
	/** The the maximum allowed distance from the starting y position */
	private int maxWave = 24;
	
	/** The amount of x movement in each update */
	private int partialWave = 8;
	
	/** The constant representing the down direction */
	private final int DOWN = 1;
	
	/** The constant representing the up direction. */
	private final int UP = -1;
	
	/** The wave direction. */
	private int waveDirection = DOWN;
	
	/** The x direction. */
	private int xDirection;
	
	/** The initial x coordinate. */
	private int initialX;
	
	/** The Constant WAVEYSPEED. */
	private static final int WAVEYSPEED = 12;
	
	/** The Constant WAVEYRECHARGE is the recharge rate. */
	private static final int WAVEYRECHARGE = 10;
	
	/** The Constant WAVEYRANGE is the maximum x distance. */
	private static final int WAVEYRANGE = 250;

	/**
	 * Instantiates a new wavey laser.
	 *
	 * @param x the x
	 * @param y the y
	 * @param direction the direction (either 1 for right, -1 for left)
	 */
	public WaveyLaser(int x, int y, int direction) {
		super("res/waveyLaser.png", x, y, direction * WAVEYSPEED);
		xDirection = direction;
		initialY = y;
		initialX = x;
		setRechargeRate(WAVEYRECHARGE);

	}

	/** The wavey laser moves in a wave to the left or right, then quickly dies.
	 * @see com.kyejoe.weapons.Laser#move()
	 */
	@Override
	public void move() {
		testRange();
		super.move();
		if (waveDirection == UP) {
			if (getyLoc() > initialY - maxWave)
				setyLoc(getyLoc() - partialWave);
			else
				waveDirection = DOWN;
		} else {
			if (getyLoc() < initialY + maxWave)
				setyLoc(getyLoc() + partialWave);
			else
				waveDirection = UP;
		}
	}

	/**
	 * Tests whether this laser is further than its max range, and then removes it.
	 */
	private void testRange() {
		if (xDirection == 1) {
			if (getxLoc() > initialX + WAVEYRANGE)
				this.remove();
		} else {
			if (getxLoc() < initialX - WAVEYRANGE)
				this.remove();
		}

	}
}
