package com.kyejoe;

import static com.kyejoe.GameComponent.frame;

import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.HashSet;

import com.kyejoe.events.GameEvent;
import com.kyejoe.weapons.Attack;

/**
 * The Class Updater is a runnable meant to be run by a ScheduledThreadPool.
 * Each time it is run, multiple times per second, it will update the position
 * of each object on the screen, and then forward a repaint request to the
 * EventQueue thread
 * 
 * @author Joe Targia and Kye Shibata
 */
public class Updater implements Runnable {

	/** The main GameComponent panel. */
	// private GameComponent panel = GameComponent.getComponent();
	/** The set of objects that should be removed. */
	private HashSet<GameObject> deadSet = new HashSet<GameObject>();

	/** The set of events that should be removed. */
	private HashSet<GameEvent> doneEvents = new HashSet<GameEvent>();
	
	/** True if the game is paused. */
	private boolean paused = false;

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Runnable#run()
	 */
	@Override
	public void run() {
		if (!paused) {
			for (GameObject target : frame.getGameObjects()) {
				target.updateLocation();
				if (target.isMarkedForRemoval())
					deadSet.add(target);
			}

			frame.incGameTimeScore();

			for (GameEvent event : frame.getGameEvents())
			{
				if ((event.getHitBox().intersects(frame.getMogo().getHitBox()) || event.isOn()) && !event.isOver())
				{
					event.eventCall();
					if (!event.getAdditionalObjects().isEmpty())
					{
						frame.getGameObjects().addAll(event.getAdditionalObjects());
						event.getAdditionalObjects().clear();
					}
					
					
					
				} else if (event.isOver())
				{
					doneEvents.add(event);
					frame.getGameObjects().removeAll(event.getAdditionalObjects());
					
				}
				
			}
			
			if (frame.getMogo().getxLoc() > frame.getWidth() - 300
					|| frame.getMogo().getxLoc() < 300) {
				for (GameObject target : frame.getGameObjects()) {

					target.setxLoc(target.getxLoc() + -1
							* frame.getMogo().getxChange());
				}
				for (GameEvent event : frame.getGameEvents())
				{
					event.setxLoc(event.getxLoc() + -1 * frame.getMogo().getxChange());
					event.updateHitBox();
				}
				
				frame.getBack().shift(frame.getMogo().getxChange());
			}
			if (!(deadSet.isEmpty())) {
				frame.getGameObjects().removeAll(deadSet);
				deadSet.clear();
			}
			
			if (!(doneEvents.isEmpty())) {
				frame.getGameEvents().removeAll(doneEvents);
				doneEvents.clear();
			}
			ArrayList<Attack> newAttacks = frame.getMogo().getNewAttacks();

			if (!(newAttacks.isEmpty())) {
				frame.getGameObjects().addAll(newAttacks);
				newAttacks.clear();
			}
			if (frame.getMogo().getHitBox()
					.intersects(frame.getBack().getFinishPoint().getEndBox())) {

				frame.wonGame();
			} else if (frame.getMogo().isDead()) {
				frame.lostGame();
			
			}
			
		}
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				frame.repaint();
			}
		});
	}
	/**
	 * Toggle paused.
	 */
	public void togglePaused() {
		this.paused = !this.paused;
		
	}

}
