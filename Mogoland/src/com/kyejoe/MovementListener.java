package com.kyejoe;

import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.HashSet;

import static com.kyejoe.InputConstants.*;

/**
 * The listener interface for receiving movement events. The class that is
 * interested in processing a movement event implements this interface, and the
 * object created with that class is registered with a component using the
 * component's <code>addMovementListener<code> method. When
 * the movement event occurs, that object's appropriate
 * method is invoked.
 * 
 * 
 * @author Joseph Targia
 * 
 */
public class MovementListener extends KeyAdapter {

	/** The current keys is the set of currently pressed keys. */
	private static HashSet<Integer> currentKeys = new HashSet<Integer>();

	/** true if a key is currently pressed. */
	private static boolean pressed = false;

	/** The Constant keys is a set of possible inputs. */
	public static final HashSet<Integer> keys = new HashSet<Integer>();

	/**
	 * Instantiates a new movement listener.
	 */
	public MovementListener() {
		keys.add(LEFT);
		keys.add(RIGHT);
		keys.add(UP);
		keys.add(DOWN);
		keys.add(JUMP);
		keys.add(SHOOT);
		keys.add(SWITCH);
		keys.add(ESC);
	}

	/**
	 * This method overrides the keyPressed method. It adds the pressed key
	 * to the set of active keys.
	 *  
	 * @see java.awt.event.KeyAdapter#keyPressed(java.awt.event.KeyEvent)
	 **/
	@Override
	public void keyPressed(KeyEvent e) {
		Integer key = e.getKeyCode();
		if (key == ESC) {
			GameComponent.frame.pause();
		}
		if (keys.contains(key)) {
			currentKeys.add(key);
			pressed = true;
		}
	}

	/**
	 * This method overrides the keyReleased method. It removes the released key
	 * from the set of active keys.
	 * 
	 * @see java.awt.event.KeyAdapter#keyReleased(java.awt.event.KeyEvent)
	 **/
	@Override
	public void keyReleased(KeyEvent e) {
		Integer key = e.getKeyCode();
		if (keys.contains(key)) {
			currentKeys.remove(key);
			if (currentKeys.isEmpty())
				pressed = false;
		}
	}

	/**
	 * Gets the currently pressed keys.
	 * 
	 * @return the current key
	 */
	public static HashSet<Integer> getCurrentKey() {
		return currentKeys;
	}

	/**
	 * Checks if any key is current pressed.
	 * 
	 * @return true, if a key is pressed
	 */
	public static boolean isPressed() {

		return pressed;
	}

}
