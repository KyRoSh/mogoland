package com.kyejoe;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.Toolkit;
import javax.swing.JFrame;

import com.kyejoe.GameComponent;

// TODO: Auto-generated Javadoc
/**
 * The Class Start contains the main method, and initializes the JFrame used by
 * the program.
 * 
 * @author Joseph Targia
 */
public class Start {

	/**
	 * The main method initializes the GameFrame, and sets its properties,
	 * ensuring the window won't be wider than the screen.
	 * 
	 * @param args the command line arguments. These are not used in this
	 *            program.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			@Override
			public void run() {
				GameFrame frame = new GameFrame();
				frame.add(GameComponent.frame, BorderLayout.CENTER);
				frame.setVisible(true);
				Toolkit kit = Toolkit.getDefaultToolkit();
				Dimension screenSize = kit.getScreenSize();
				int screenWidth = screenSize.width;
				if (screenWidth > 1000)
					screenWidth = 1000;

				frame.setSize(new Dimension(screenWidth, 500 + frame
						.getInsets().top));
				frame.setResizable(false);
			}
		});
	}
}

@SuppressWarnings("serial")
class GameFrame extends JFrame {
	/**
	 * Instantiates a new GameFrame which is a top level window. Nothing is drawn
	 * directly on the window.
	 */
	public GameFrame() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationByPlatform(true);
	}
}