package com.kyejoe.events;

import com.kyejoe.creatures.Walrus;
import com.kyejoe.levels.Platform;

// TODO: Auto-generated Javadoc
/**
 * The Class WalrusFrenzy is a GameEvent, acting as a secret boss in the
 * FirstLevel. It will cause a giant horde of "walruses" to appear when the
 * player reaches the given coordinates.
 * 
 * @author Kye Shibata
 */
public class WalrusFrenzy extends GameEvent {

	/**
	 * Instantiates a new walrus frenzy at the given location.
	 * 
	 * @param xLoc the x loc
	 * @param yLoc the y loc
	 */
	public WalrusFrenzy(int xLoc, int yLoc) {
		super(xLoc, yLoc, 50, 500);
	}

	/**
	 * wall1~wall3 act as the left walls for the boss battle.
	 */
	private Platform wall1, wall2, wall3;

	/**
	 * Calls the event, spawning many "walrus" enemies to attack the player, and
	 * locking him in with rocks on the right
	 */
	@Override
	public void eventCall() {

		if (!eventOn) {

			eventOn = true;
			array.add(new Walrus(500, 0));
			array.add(new Walrus(400, 0));
			array.add(new Walrus(300, 0));
			array.add(new Walrus(450, 0));
			array.add(new Walrus(550, 0));
			array.add(new Walrus(600, 0));
			array.add(new Walrus(700, 0));
			array.add(new Walrus(500, 100));
			array.add(new Walrus(500, 200));
			array.add(new Walrus(600, 100));
			array.add(new Walrus(700, 100));
			array.add(new Walrus(800, 100));
			array.add(new Walrus(900, 200));

			wall1 = new Platform("res/rock.png", -200, 342);
			wall2 = new Platform("res/rock.png", -200, 284);
			wall3 = new Platform("res/rock.png", -200, 226);

			array.add(wall1);
			array.add(wall2);
			array.add(wall3);
		}

		else if (eventOn && Walrus.getCount() == 0) {

			endEvent();
			array.add(wall1);
			array.add(wall2);
			array.add(wall3);
		}

	}

}
