package com.kyejoe.events;

import java.awt.geom.Rectangle2D;
import java.util.ArrayList;

import com.kyejoe.GameObject;

/**
 * The Class GameEvent is an abstract class that allows a sequence of actions to
 * occur when the player reaches a certain point. It is particularly useful for
 * Boss Battles and dynamic enemy spawning.
 * 
 * @author Kye Shibata
 */
public abstract class GameEvent {

	/** The upper left x location. */
	protected int xLoc = 20;

	/** The upper left y location. */
	protected int yLoc = 20;

	/** The width of the event. */
	protected int width;

	/** The height of the event. */
	protected int height;

	/** An array of objects to be added by the event. */
	protected ArrayList<GameObject> array = new ArrayList<GameObject>();

	/** The hit box is used to calculate collisions between objects. */
	protected Rectangle2D hitBox;

	/**
	 * Denotes that the event has started.
	 */
	protected boolean eventOn = false;

	/**
	 * The marked for removal flag is used to determine whether an object should
	 * be removed from updates.
	 */
	protected boolean markedForRemoval = false;

	/**
	 * Gets the upper left x location.
	 * 
	 * @return the x location
	 */
	public synchronized int getxLoc() {
		return xLoc;
	}

	/**
	 * Gets the upper left y location.
	 * 
	 * @return the y location
	 */
	public synchronized int getyLoc() {
		return yLoc;
	}

	/**
	 * Sets the upper left x location.
	 * 
	 * @param x the new x location
	 */
	public void setxLoc(int x) {
		this.xLoc = x;
	}

	/**
	 * Sets the upper left y location.
	 * 
	 * @param y the new y location
	 */
	public void setyLoc(int y) {
		this.yLoc = y;
	}

	/**
	 * Gets the hit box, which is the Rectangle2D object that corresponds to the
	 * coordinates that other GameObjects would collide with.
	 * 
	 * @return the hit box
	 */
	public Rectangle2D getHitBox() {
		return hitBox;
	}

	/**
	 * Event call causes the event to actually occur. Whatever action the
	 * GameEvent is supposed to cause will be specified here.
	 */
	public abstract void eventCall();

	/**
	 * Instantiates a new game event.
	 * 
	 * @param xLoc the x loc
	 * @param yLoc the y loc
	 * @param width the width
	 * @param height the height
	 */
	public GameEvent(int xLoc, int yLoc, int width, int height) {
		this.xLoc = xLoc;
		this.yLoc = yLoc;
		this.width = width;
		this.height = height;
		hitBox = new Rectangle2D.Double(xLoc + width, yLoc + height, width,
				height);
	}

	/**
	 * Updates the hitbox.
	 */

	public void updateHitBox() {
		hitBox = new Rectangle2D.Double(xLoc, yLoc, width, height);
	}

	/**
	 * Ends the event and marks it for removal.
	 */
	public void endEvent() {
		markedForRemoval = true;
		eventOn = false;
	}

	/**
	 * Tells whether the event has ended.
	 * 
	 * @return True if event is marked for removal
	 */
	public boolean isOver() {
		return markedForRemoval;
	}

	/**
	 * Tells whether the event is currently occuring.
	 * 
	 * @return true if the event is on
	 */
	public boolean isOn() {
		return eventOn;
	}

	/**
	 * returns the array of objects to be added to the overal game objects.
	 * 
	 * @return the array of objects to be added
	 */
	public ArrayList<GameObject> getAdditionalObjects() {
		return array;
	}
}
